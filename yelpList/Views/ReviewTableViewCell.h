//
//  ReviewTableViewCell.h
//  yelpList
//
//  Created by Gihan Deshapriya on 21/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface ReviewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingsView;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;

@end
