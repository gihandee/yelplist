//
//  BusinessTableViewCell.m
//  yelpList
//
//  Created by Gihan Deshapriya on 18/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "BusinessTableViewCell.h"

@implementation BusinessTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
