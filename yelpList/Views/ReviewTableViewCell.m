//
//  ReviewTableViewCell.m
//  yelpList
//
//  Created by Gihan Deshapriya on 21/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "ReviewTableViewCell.h"

@implementation ReviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
