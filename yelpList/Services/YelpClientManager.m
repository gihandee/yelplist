//
//  YelpClientManager.m
//  yelpList
//
//  Created by Gihan Deshapriya on 19/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "YelpClientManager.h"
@import YelpAPI;

@interface YelpClientManager () {}
    @property (strong, nonatomic) YLPClient *client;
@end

NSString * const appId = @"HPAT_99ZK_XuYrmkESloRw";
NSString * const secret = @"H0tymkY1pY4vVxrU0wsBjEx0f0j6ztKDer93httrWF4iYQwL5NVtLeZ0tL2oYbGe";

@implementation YelpClientManager

static YelpClientManager *sharedMyManager;

+(YelpClientManager *)sharedYelpClientManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(sharedMyManager == nil){
            sharedMyManager = [[self alloc] init];
        }
    });
    return sharedMyManager;
}

- (void)authorize:(void (^)(BOOL result))completionHandler {
    [YLPClient authorizeWithAppId:appId secret:secret completionHandler:^(YLPClient *client, NSError *error) {
        self.client = client;
        BOOL result = YES;
        if (!client) {
            result = NO;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(result);
        });
    }];
}

- (void)searchWithLocation:(NSString *)location
                sortOption:(NSInteger)sortOption
                completion:(void (^)(NSArray *results))completionHandler {
    [self.client searchWithLocation:location term:nil limit:10 offset:0 sort:sortOption completionHandler:^
     (YLPSearch *search, NSError* error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             completionHandler(search.businesses);
         });
     }];
}

- (void)businessWithIdentifier:(NSString *)identifier
                completion:(void (^)(YLPBusiness *business))completionHandler{
    [self.client businessWithId:identifier completionHandler:^
     (YLPBusiness *business, NSError* error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             completionHandler(business);
         });
     }];
}

- (void)reviewsWithIdentifier:(NSString *)identifier
                   completion:(void (^)(YLPBusinessReviews *review))completionHandler {
    [self.client reviewsForBusinessWithId:identifier completionHandler:^(YLPBusinessReviews * _Nullable reviews, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(reviews);
        });
    }];
}

@end
