//
//  YelpClientManager.h
//  yelpList
//
//  Created by Gihan Deshapriya on 19/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YLPClient;
@class YLPBusiness;
@class YLPBusinessReviews;

@interface YelpClientManager : NSObject

+ (YelpClientManager *)sharedYelpClientManager;
- (void)authorize:(void (^)(BOOL result))completionHandler;
- (void)searchWithLocation:(NSString *)location
                sortOption:(NSInteger)sortOption
                completion:(void (^)(NSArray *results))completionHandler;
- (void)businessWithIdentifier:(NSString *)identifier
                    completion:(void (^)(YLPBusiness *business))completionHandler;
- (void)reviewsWithIdentifier:(NSString *)identifier
                    completion:(void (^)(YLPBusinessReviews *review))completionHandler;
@end
