//
//  Constants.m
//  yelpList
//
//  Created by Gihan Deshapriya on 21/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString *const ERROR = @"Error";
NSString *const AUTH_FAIL_MASSAGE = @"Authorization faliure.";
NSString *const RETRY = @"Retry";
NSString *const CANCEL = @"CANCEL";
NSString *const BEST_MATCH = @"Best Match";
NSString *const DISTANCE = @"Distance";
NSString *const HIGHEST_RATED = @"Highest Rated";
NSString *const MOST_REVIEWS = @"Most Reviewed";
NSString *const SORT = @"Sort";
NSString *const CONFIRM = @"Confirm";
@end
