//
//  Constants.h
//  yelpList
//
//  Created by Gihan Deshapriya on 21/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

extern NSString *const ERROR;
extern NSString *const AUTH_FAIL_MASSAGE;
extern NSString *const RETRY;
extern NSString *const CANCEL;
extern NSString *const BEST_MATCH;
extern NSString *const DISTANCE;
extern NSString *const HIGHEST_RATED;
extern NSString *const MOST_REVIEWS;
extern NSString *const SORT;
extern NSString *const CONFIRM;
@end
