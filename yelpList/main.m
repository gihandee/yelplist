//
//  main.m
//  yelpList
//
//  Created by Gihan Deshapriya on 17/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
