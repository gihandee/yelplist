//
//  SearchViewController.m
//  yelpList
//
//  Created by Gihan Deshapriya on 18/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "SearchViewController.h"
#import "BusinessTableViewCell.h"
#import "YelpClientManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <YelpAPI/YelpAPI.h>
#import "CZPicker.h"
#import <HCSStarRatingView/HCSStarRatingView.h>
#import "DetailViewController.h"
#import "SVProgressHUD.h"
#import "Constants.h"

@interface SearchViewController () < UITableViewDelegate , UITableViewDataSource, UISearchBarDelegate, CZPickerViewDelegate, CZPickerViewDataSource> {
   UISearchBar *_searchBar;
    __weak IBOutlet UITableView *_searchTableView;
    NSArray *_results;
    NSArray *_sortValues;
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    _searchTableView.delegate = self;
    _searchTableView.dataSource = self;
    //Do intial search and setup searchbar
    [self searchWithText:@"Mississauga, ON"];
     _searchTableView.estimatedRowHeight = 130.0;
    
    _searchBar = [[UISearchBar alloc] init];
    _searchBar.delegate = self;
    [_searchBar sizeToFit];
    self.navigationItem.titleView = _searchBar;
    _searchBar.text = @"Mississauga, ON";
    
    _sortValues = [NSArray arrayWithObjects:BEST_MATCH,DISTANCE,HIGHEST_RATED,MOST_REVIEWS, nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView Delegate methods.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _results.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Dequeue cells and assign related data.
    BusinessTableViewCell *cell = (BusinessTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"BusinessTableViewCell" forIndexPath:indexPath];
    YLPBusiness *business = ((YLPBusiness *)_results[indexPath.row]);
    cell.nameLabel.text = business.name;
    [cell.businessImageView sd_setImageWithURL:business.imageURL
                 placeholderImage:nil];
    cell.distanceLabel.text = [NSString stringWithFormat:@"%.2f mi" , business.distance * 0.000621371];
    [cell.ratingsView setValue:business.rating];
    cell.reviewCountLabel.text = [NSString stringWithFormat:@"%lu Reviews", (unsigned long)business.reviewCount];
    if(business.location.address.count > 1) {
        cell.addressLabel.text = [NSString stringWithFormat:@"%@, %@",business.location.address[1],business.location.address[0]];
    } else if(business.location.address.count == 1) {
        cell.addressLabel.text  = business.location.address[0];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_searchBar resignFirstResponder];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailViewController *controller = (DetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    controller.identifier = ((YLPBusiness *)_results[indexPath.row]).identifier;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_searchBar resignFirstResponder];
}

#pragma mark - UISearchBar delegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [_searchBar resignFirstResponder];
    [self searchWithText:searchBar.text];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.text = @"";
    return YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}

/**
 Search with text

 @param text search text
 */
- (void)searchWithText:(NSString *)text {
    [self searchWithText:text sortOption:1];
}

/**
 Search with text

 @param text search text
 @param sortOption sortOption
 */
- (void)searchWithText:(NSString *)text sortOption:(NSInteger)sortOption {
    [SVProgressHUD show];
    [[YelpClientManager sharedYelpClientManager] searchWithLocation:text sortOption:sortOption completion:^(NSArray *results) {
        [SVProgressHUD dismiss];
        _results = results;
        [_searchTableView reloadData];
    }];
}

#pragma mark - CzpickerView delegate methods
- (void)czpickerView:(CZPickerView *)pickerView
didConfirmWithItemAtRow:(NSInteger)row {
    [self searchWithText:_searchBar.text sortOption:row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    return _sortValues.count;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return _sortValues[row];
}

- (IBAction)sortAction:(id)sender {
    [_searchBar resignFirstResponder];
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:SORT
                                                   cancelButtonTitle:CANCEL
                                                  confirmButtonTitle:CONFIRM];
    picker.headerBackgroundColor = [UIColor colorWithRed:(196/255.0) green:(18/255.0) blue:(0/255.0) alpha:1.0];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = NO;
    [picker show];
}

@end
