//
//  DetailViewController.m
//  yelpList
//
//  Created by Gihan Deshapriya on 19/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "DetailViewController.h"
#import "YelpClientManager.h"
#import <YelpAPI/YelpAPI.h>
#import <HCSStarRatingView/HCSStarRatingView.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MWPhotoBrowser.h"
#import "MapViewController.h"
#import "SVProgressHUD.h"
#import "ReviewViewController.h"

@interface DetailViewController () <MWPhotoBrowserDelegate> {
    YLPBusiness *_business;
    __weak IBOutlet HCSStarRatingView *_ratingsVIew;
    __weak IBOutlet UILabel *_reviewCountLabel;
    __weak IBOutlet UILabel *_categoriesLabel;
    __weak IBOutlet UILabel *_telephoneNumberLabel;
    __weak IBOutlet UILabel *_addressLabel;
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UIImageView *_gallaryImageView1;
    __weak IBOutlet UIImageView *_gallaryImageView2;
    __weak IBOutlet UIImageView *_gallaryImageView3;
    IBOutlet UITapGestureRecognizer *_gallaryTap;
    __weak IBOutlet UIView *_overlayView;
    NSMutableArray *_photos;
    NSMutableArray *_thumbs;
}
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self retrieveData];
}

/**
 Retreive business data.
 */
-(void)retrieveData {
    [_overlayView setHidden:NO];
    [SVProgressHUD show];
    [[YelpClientManager sharedYelpClientManager] businessWithIdentifier:_identifier completion:^(YLPBusiness *business) {
        [SVProgressHUD dismiss];
        if(business) {
            _business = business;
            [_overlayView setHidden:YES];
            [self updateView];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 Update view with recieved data.
 */
-(void)updateView {
    self.navigationItem.title = _business.name;
    [_ratingsVIew setValue:_business.rating];
    [_imageView sd_setImageWithURL:_business.imageURL
                              placeholderImage:nil];
    _reviewCountLabel.text = [NSString stringWithFormat:@"%lu Reviews", (unsigned long)_business.reviewCount];
    _addressLabel.text = [self getAddress];
    _telephoneNumberLabel.text = _business.phone;
    NSString *str = @"";
    for (YLPCategory *cat in _business.categories) {
        if (str.length == 0) {
            str = cat.name;
        } else {
            str = [NSString stringWithFormat:@"%@, %@", str , cat.name];
        }
    }
    _categoriesLabel.text = str;
    [self setGallaryImages];
}

/**
 Comile the address

 @return return address string
 */
-(NSString *)getAddress {
    NSString *address = @"";
    if(_business.location.address.count > 1) {
        address = [NSString stringWithFormat:@"%@, %@",_business.location.address[1],_business.location.address[0]];
    } else if(_business.location.address.count == 1) {
         address = _business.location.address[0];
    }
    if(_business.location.city) {
        address = [NSString stringWithFormat:@"%@, %@",address,_business.location.city];
    }
    if(_business.location.postalCode) {
        address = [NSString stringWithFormat:@"%@, %@",address,_business.location.postalCode];
    }
    return address;
}


/**
 Set images for the details view.
 */
-(void)setGallaryImages {
    if(_business.photos.count > 0) {
        if(_business.photos[0]) {
            [_gallaryImageView1 sd_setImageWithURL:_business.photos[0]
                          placeholderImage:nil];
        }
        if(_business.photos.count > 1) {
            [_gallaryImageView2 sd_setImageWithURL:_business.photos[1]
                                  placeholderImage:nil];
        }
        if(_business.photos.count > 2) {
            [_gallaryImageView3 sd_setImageWithURL:_business.photos[2]
                                  placeholderImage:nil];
        }
    }
}


/**
 Gallary tap action. The full screen gallary view of images are handled here.

 @param sender sender
 */
- (IBAction)gallaryTapAction:(id)sender {
    _photos = [[NSMutableArray alloc] init];
    _thumbs = [[NSMutableArray alloc] init];
    for(NSString *url in _business.photos) {
        MWPhoto *photo = [MWPhoto photoWithURL:[[NSURL alloc] initWithString:url]];
        photo.caption = _business.name;
        [_photos addObject:photo];
        [_thumbs addObject:[MWPhoto photoWithURL:[[NSURL alloc] initWithString:url]]];
    }
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = NO;
    browser.displayNavArrows = YES;
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = NO;
    browser.startOnGrid = NO;
    browser.enableSwipeToDismiss = NO;
    browser.autoPlayOnAppear = NO;
    [browser setCurrentPhotoIndex:0];
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/**
 Navigate to map view.

 @param sender sender
 */
- (IBAction)mapClicked:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapViewController *controller = (MapViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    controller.latitude = [NSNumber numberWithDouble:_business.location.coordinate.latitude];
    controller.longitude = [NSNumber numberWithDouble:_business.location.coordinate.longitude];
    controller.name = _business.name;
    [self.navigationController pushViewController:controller animated:YES];
}


/**
 Rerty option for a request failiure.

 @param sender sender
 */
- (IBAction)overlayRetry:(id)sender {
    [self retrieveData];
}

/**
 Navigate to reviews view.

 @param sender sender
 */
- (IBAction)reviewsButtonClicked:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ReviewViewController *controller = (ReviewViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ReviewViewController"];
    controller.identifier = _identifier;
    [self.navigationController pushViewController:controller animated:YES];
}

@end
