//
//  ReviewViewController.m
//  yelpList
//
//  Created by Gihan Deshapriya on 21/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "ReviewViewController.h"
#import "YelpClientManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <YelpAPI/YelpAPI.h>
#import "ReviewTableViewCell.h"

@interface ReviewViewController ()< UITableViewDelegate , UITableViewDataSource> {
    
    __weak IBOutlet UITableView *reviewTableview;
    YLPBusinessReviews *_reviews;
}

@end

@implementation ReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    reviewTableview.delegate = self;
    reviewTableview.dataSource = self;
    reviewTableview.estimatedRowHeight = 130.0;
    [self getReview];
    // Do any additional setup after loading the view.
}

- (void)getReview {
    [[YelpClientManager sharedYelpClientManager] reviewsWithIdentifier:_identifier completion:^(YLPBusinessReviews *reviews) {
        _reviews = reviews;
        [reviewTableview reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _reviews.reviews.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReviewTableViewCell *cell = (ReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ReviewTableViewCell" forIndexPath:indexPath];
    YLPReview *review = ((YLPReview *)_reviews.reviews[indexPath.row]);
    cell.nameLabel.text = review.user.name;
    [cell.userImage sd_setImageWithURL:review.user.imageURL
                              placeholderImage:nil];

    [cell.ratingsView setValue:review.rating];
    cell.reviewLabel.text = review.excerpt;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


@end
