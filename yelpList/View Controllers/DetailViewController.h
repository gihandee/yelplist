//
//  DetailViewController.h
//  yelpList
//
//  Created by Gihan Deshapriya on 19/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NSString *identifier;

@end
