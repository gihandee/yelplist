//
//  IntialViewController.m
//  yelpList
//
//  Created by Gihan Deshapriya on 19/11/2017.
//  Copyright © 2017 gajayaCorp. All rights reserved.
//

#import "InitialViewController.h"
#import "SearchViewController.h"
#import "YelpClientManager.h"
#import "SVProgressHUD.h"
#import "Constants.h"

@interface InitialViewController () {
    UIAlertController * alert;
}

@end

@implementation InitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupAlertController];
    [self authenticate];
}

/**
 Authenticate (authorize) the client
 */
- (void)authenticate {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD setBackgroundLayerColor:[UIColor whiteColor]];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:(196/255.0) green:(18/255.0) blue:(0/255.0) alpha:1.0]];
    [SVProgressHUD show];
    [[YelpClientManager sharedYelpClientManager] authorize:^(BOOL result) {
        [SVProgressHUD dismiss];
        if(result) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *controller = (SearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        } else {
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}


/**
 Setup alert controller to handle retry connection in case of faliure.
 */
- (void)setupAlertController {
    alert =   [UIAlertController alertControllerWithTitle:ERROR
                                  message:AUTH_FAIL_MASSAGE
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [SVProgressHUD show];
                             [self authenticate];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:CANCEL
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
